package ru.stiznt.rnd_contest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.stiznt.rnd_contest.API.API;
import ru.stiznt.rnd_contest.API.ConfirmedPrintJob;
import ru.stiznt.rnd_contest.API.PrintTask;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MainWindow extends JFrame {

    private int DELAY = 5; //seconds


    private Object headers[] = new String[] {"printed","id", "user", "contest", "time"};
    private JPanel rootPane;
    private JButton print;
    private JButton view;
    private JTable table;
    private JButton printed;
    private List<PrintTask> tasks;
    private List<Integer> printedId = new ArrayList<>();
    private API api;

    public MainWindow(){
         Retrofit retrofit = new Retrofit.Builder().baseUrl("http://judge.rndcontest.ru/api/").addConverterFactory(GsonConverterFactory.create()).build();
         api = retrofit.create(API.class);

         Timer timer = new Timer(DELAY * 1000, new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {

                 try{
                     ConfirmedPrintJob job = new ConfirmedPrintJob();
                     job.setPrinted(printedId);
                     api.sendConfirm(job).execute();
                     Thread.sleep(100);

                     tasks.clear();
                     tasks.addAll(api.getTasks().execute().body());
                     printedId.clear();
                     table.updateUI();
                 }catch (IOException ex){
                     ex.printStackTrace();
                 }catch (InterruptedException ex){
                     ex.printStackTrace();
                 }

             }
         });

         timer.start();

         setContentPane(rootPane);
         setSize(750, 500);
         loadTasks();
         tasks = new ArrayList<>();
         table.setModel(new TableModel());

         createHeaders();

         addListeners();


         setDefaultCloseOperation(EXIT_ON_CLOSE);
         setVisible(true);
    }

    private void loadTasks(){
        api.getTasks().enqueue(new Callback<List<PrintTask>>() {
            @Override
            public void onResponse(Call<List<PrintTask>> call, Response<List<PrintTask>> response) {
                if(response.body() != null){
                    tasks.clear();
                    tasks.addAll(response.body());
                    table.updateUI();
                }
            }

            @Override
            public void onFailure(Call<List<PrintTask>> call, Throwable t) {

            }
        });
    }

    private void createHeaders(){
        TableColumnModel columnModel = table.getColumnModel();
        Enumeration<TableColumn> e = columnModel.getColumns();
        int a = 0;
        while(e.hasMoreElements()){
            TableColumn column = e.nextElement();
            column.setHeaderValue(headers[a]);
            a++;
        }
    }



    private void addListeners(){

        printed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] rows = table.getSelectedRows();
                for(int i = 0; i < rows.length; i++){
                    tasks.get(i).setPrinted(true);
                    printedId.add(tasks.get(i).getId());
                    //lastPrinted.remove(rows[i]);
                }
                table.updateUI();
            }
        });

        view.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int currentRow = table.getSelectedRow();
                if(currentRow != -1) new PrintJobView(tasks.get(currentRow).getContent());
            }
        });

        print.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JEditorPane editor = new JEditorPane();
                editor.setContentType("text/html");
                editor.setText(tasks.get(table.getSelectedRow()).getContent());
                MessageFormat footer = new MessageFormat(" Страница №{0,number,integer}");
                MessageFormat header = new MessageFormat("");
                try {
                    editor.print(header, footer, true, null, null, false);
                }catch (PrinterException ex){
                    ex.printStackTrace();
                }
            }
        });
    }

    class TableModel extends AbstractTableModel{

        @Override
        public int getRowCount() {
            return tasks.size();
        }

        @Override
        public int getColumnCount() {
            return headers.length;
        }

        @Override
        public Class<?> getColumnClass(int column) {
            if(column == 0) return Boolean.class;
            return Object.class;
        }

        @Override
        public Object getValueAt(int row, int column) {

            switch(column){
                case 0: return tasks.get(row).isPrinted();
                case 1: return tasks.get(row).getId();
                case 2: return tasks.get(row).getTeam();
                case 3: return tasks.get(row).getProblem();
                case 4: return tasks.get(row).getCreatedAt();
            }

            return "4chan";
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            if(columnIndex == 0) return true;
            return false;
        }
    }

}
