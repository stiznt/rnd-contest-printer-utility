package ru.stiznt.rnd_contest;

import javax.swing.*;

public class PrintJobView extends JFrame{
    private JTextPane text;
    private JPanel rootPane;

    public PrintJobView(String content){
        setContentPane(rootPane);
        setSize(500, 500);
        text.setText(content);

        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
