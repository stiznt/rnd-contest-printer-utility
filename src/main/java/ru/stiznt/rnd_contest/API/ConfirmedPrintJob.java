package ru.stiznt.rnd_contest.API;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmedPrintJob {

    @SerializedName("printing")
    @Expose
    private List<Integer> printing = null;
    @SerializedName("printed")
    @Expose
    private List<Integer> printed = null;
    @SerializedName("failed")
    @Expose
    private List<Integer> failed = null;

    public List<Integer> getPrinting() {
        return printing;
    }

    public void setPrinting(List<Integer> printing) {
        this.printing = printing;
    }

    public List<Integer> getPrinted() {
        return printed;
    }

    public void setPrinted(List<Integer> printed) {
        this.printed = printed;
    }

    public List<Integer> getFailed() {
        return failed;
    }

    public void setFailed(List<Integer> failed) {
        this.failed = failed;
    }

}

