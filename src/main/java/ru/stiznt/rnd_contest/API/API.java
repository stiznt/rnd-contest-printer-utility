package ru.stiznt.rnd_contest.API;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;

import java.util.List;

public interface API {

    @GET("print/pending")
    Call<List<PrintTask>> getTasks();

    @PUT("print/confirm")
    Call<ConfirmedPrintJob> sendConfirm(@Body ConfirmedPrintJob job);

}
