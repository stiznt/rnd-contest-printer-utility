package ru.stiznt.rnd_contest.API;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrintTask {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private String team;
    @SerializedName("contest")
    @Expose
    private String problem;
    @SerializedName("time")
    @Expose
    private String createdAt;
    @SerializedName("content")
    @Expose
    private String content;

    private boolean isPrinted;

    public boolean isPrinted(){
        return isPrinted;
    }

    public void setPrinted(boolean kek){
        isPrinted = kek;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
